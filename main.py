# from imutils.video import VedioStream
from flask import Response,Flask,render_template
import cv2

def frameGeneration():
    capture = cv2.VideoCapture(0)
    while(True):
        ret, frame = capture.read()
        # cv2.imshow('frame', frame)
        if not ret:
            break
        else:
            ret2, buffer = cv2.imencode(".jpg", frame)
            frame = buffer.tobytes()
            yield (b'--frame\r\n'
                   b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n')
        # if cv2.waitKey(20) & 0xFF == ord('q'):
        #     break
    capture.release()


app = Flask(__name__)

@app.route('/')
def index():
    return Response(frameGeneration(), mimetype='multipart/x-mixed-replace; boundary=frame')

if __name__ == "__main__":
    # frameGeneration()
    app.run(debug=True)